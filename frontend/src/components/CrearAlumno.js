import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2';
import {withRouter} from 'react-router-dom'

const CrearAlumno = (props) => {

  return (
    <div className="my-5">
      <div className="card mb-3 shadow p-3 mb-5 bg-white rounded">
        <div className="row no-gutters">
          <div className="col-md-4 d-flex justify-content-center align-items-center">
            <i className="fas fa-users fa-10x text-primary"></i>
          </div>
          <div className="col-md-8">
            <div className="card-body">
              <form >
                <div className="form-group">
                  <label>Nombre </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ingrese el nombre"
                   
                  />
                </div>
                <div className="form-group">
                  <label>Apellido</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ingrese el apellido"
                  
                  />
                </div>
                <div className="form-group">
                  <label>Legajo</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ingrese el legajo ej:101"
                 
                  />
                </div>
                <div className="form-group">
                  <label>Comision</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ingrese la comision ej: A5"
                 
                  />
                </div>
                <div className="text-right">
                  <button className="btn btn-primary" type="submit">
                    <i className="fas fa-plus-square"></i> Agregar{" "}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(CrearAlumno);
