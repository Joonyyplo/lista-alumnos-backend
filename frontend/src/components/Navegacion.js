import React from 'react';
import {Link, NavLink} from 'react-router-dom'

const Navegacion = () => {
    return (
        <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
             <Link to="/" className="navbar-brand">
                 Lista de Alumnos
            </Link>  
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <NavLink exact={true} to="/"
                    className="nav-link"
                    activeClassName="active"
                    >Alumnos</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink  exact={true} to="/alumno/nuevo"
                    className="nav-link"
                    activeClassName="active">Nuevo Alumno</NavLink>
                </li>
            </ul> 
            </div>
        </nav>
    </div>
    );
};

export default Navegacion;