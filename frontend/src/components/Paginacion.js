import React, { useState } from "react";

const Paginacion = props => {
  //Defino la cantidad de botones que necesito
  let botones = [];

  for (let i = 0; i < props.totalPaginas; i++) {
    console.log(props.currentpage)
    let active = i === props.currentpage ? "active" : "";
    botones.push(
      <li className="page-item">
        <a
          className={`page-link btn btn-primary ${active}`}
          href="#"
          onClick={
            () => props.alumnosAPI(i) //como los parametros ya estan definidos por defecto en la funcion alumnosAPI en el componente app.js no hace falta mandarle todos los parametros que espera, si no estuvieran definidos si o si hay que mandarle todos los parametros por ejemplo props.alumnosAPI(valor, valor)
          }>
          {i + 1}
        </a>
      </li>
    );
  }

  let disabledPrev = props.currentpage + 1 <= 1 ? "page-link btn btn-outline-secondary disabled" : "page-link";
  let disabledNext = props.currentpage + 1 >= props.totalPaginas ? "page-link btn btn-outline-secondary disabled" : "page-link";

  return (
    <div>
      <nav aria-label="Page navigation example">
        <ul className="pagination d-flex justify-content-center">
          <li className="page-item">
            <a className={disabledPrev} href="#" aria-label="Previous"
            onClick={() =>
              props.alumnosAPI(props.currentpage - 1)
            }>
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>

          {botones}

          <li className="page-item">
            <a className={disabledNext} href="#" aria-label="Next"
            onClick={() =>
              props.alumnosAPI(props.currentpage + 1)
            }>
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Paginacion;
