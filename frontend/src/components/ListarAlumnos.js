import React from "react";
import Alumno from "./Alumno";

const ListarAlumnos = props => {
  return (
      <div className="my-5">
        <div className="row">
          {props.alumnos.map((alumn, id) => (
            <Alumno alumn={alumn} key={id} />
          ))}
        </div>
      </div>
  );
};

export default ListarAlumnos;
