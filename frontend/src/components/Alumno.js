import React from "react";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

const Alumno = props => {
  const { nombre, apellido, legajo, curso } = props.alumn;

  return (
    <div className="col-md-3">
      <div className="card shadow mb-5 bg-white rounded">
        <h5 className="card-header text-center">Alumno</h5>
        <div className="card-body">
          <p className="text-center">
            {" "}
            <i className="fas fa-user-circle fa-8x text-primary "></i>
          </p>
          <h5 className="card-title">
            {nombre}, {apellido}
          </h5>
          <p className="card-text">Legajo: {legajo}</p>
          <p className="card-text">{curso}</p>
        </div>
        <div className="card-footer text-right">
          <Link to={`/alumno/editar/1`} className="btn btn-secondary mr-3">
            <i className="fas fa-edit"></i>
          </Link>
          <button className="btn btn-danger">
            <i className="fas fa-trash-alt"></i>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Alumno;
