import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navegacion from "./components/Navegacion";
import ListarAlumnos from "./components/ListarAlumnos";
import CrearAlumno from "./components/CrearAlumno";
import Paginacion from "./components/Paginacion";

function App() {
  const [alumnos, setAlumnos] = useState([]);
  const [quantity, setQuantity] = useState(1);
  const [currentpage, setCurrentpage] = useState(0);
  const [totalPaginas, setTotalPaginas] = useState(0);

  useEffect(() => {
    alumnosAPI();
  }, []);

  const alumnosAPI = async (
    paramCurrentPage = currentpage,
    paramQuantity = quantity
  ) => {
    console.log(paramCurrentPage)
    const request = await fetch(
      `http://localhost:4000/api/students?quantity=${quantity}&currentpage=${paramCurrentPage}`
    );
    const response = await request.json();
    console.log(request);
    console.log(response);
    setAlumnos(response.mensaje);
    setTotalPaginas(response.totalPages);
    setCurrentpage(response.currentpage);
  };

  return (
    <Router>
      <Navegacion />
      <div className="container">
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <>
                <ListarAlumnos alumnos={alumnos} />
                <Paginacion
                  alumnosAPI={alumnosAPI}
                  totalPaginas={totalPaginas}
                  currentpage={currentpage}
                />
              </>
            )}
          ></Route>
          <Route
            exact
            path="/alumno/nuevo"
            render={() => <CrearAlumno />}
          ></Route>
          <Route
            exact
            path="/alumno/editar/:id"
            component={CrearAlumno}
          ></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
