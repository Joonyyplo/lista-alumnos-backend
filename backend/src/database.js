const mongoose = require('mongoose');
console.log(process.env.MONGODB_URL)
const url = process.env.MONGODB_URL;

mongoose.connect(url, {
    useNewUrlParser: true,
    useCreateIndex: true
})

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('DB conected')
})