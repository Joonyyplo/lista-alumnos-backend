const {Schema, model} = require('mongoose');

const alumnoSchema = new Schema({
    nombre: String,
    apellido: {
        type: String,
        required: true
    },
    legajo: Number,
    curso: String
},
{
    timestamps:true
});
module.exports = model('Alumno', alumnoSchema)

