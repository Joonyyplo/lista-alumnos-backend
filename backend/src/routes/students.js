const { Router } = require('express');

const { listarAlumnos, crearAlumno, obtenerAlumno, modificarAlumno, borrarAlumno} = require('../controllers/alumnos.controllers')


const router = Router();

router.route('/')
    .get(listarAlumnos)
    .post(crearAlumno);

router.route('/:id')
.get(obtenerAlumno)
.put(modificarAlumno)
.delete(borrarAlumno);

module.exports = router;