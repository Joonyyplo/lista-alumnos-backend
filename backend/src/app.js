const express = require('express')
const cors = require('cors')

const app = express();

//settings: aqui vamos a poner configuraciones previas

app.set('port', 4000) //esto seria como escribir "const port = 4000"


//middlewares: son funciones que se van a ejecutar antes que llegue a mi ruta

app.use(cors()); // me permite que el servior interactue con el front
app.use(express.json()) // con esta funcion ahora mi servidor entiende que es un json

//routes: aqui van mis rutas

app.use('/api/students', require('./routes/students'));
// app.use('/api/contactos', (req, res) => res.send('aqui van los contactos'));

module.exports = app;