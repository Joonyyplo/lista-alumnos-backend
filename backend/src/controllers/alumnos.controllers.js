const Alumno = require('../models/Alumno');

const controladorAlumno = {};

//al objeto controladorAlumno le agrego una propiedad ListarAlumnos
controladorAlumno.listarAlumnos = async (req, res) => {
    const limit = parseInt(req.query.quantity);
    const skip = parseInt((req.query.currentpage) * limit) //con esto limitamos los objetos que apareceran en el frontend
    console.log(limit, skip);

    Alumno.countDocuments(async (err, count) => {
        await Alumno.find({}, null, {skip: skip, limit: limit}, //skip y limit son cosas que llegan por la url
            (error, alumnosdata) => { //alumnosdata es un arreglo que llega del find
                if (error){
                    res.status(500).send(err);
                    return;
                }
                res.json({
                    mensaje: alumnosdata,
                    currentpage: parseInt(req.query.currentpage),
                    totalPages: Math.ceil(count / limit) // la palabra count se puede reemplazar con otro nombre
                })
            }).sort({ _id: 1 }) // aqui se pone 1 para ordenar el registro en ordes ascendente (1, 2, 3), y si ponemos ({ _id: -1 }) ordenaria (3, 2, 1) descendente
    })
    // const listaAlumnos = await Alumno.find();
    // res.json(listaAlumnos)
};


controladorAlumno.crearAlumno = async(req, res) => {
    console.log(req.body);
    const { nombre, apellido, legajo, curso} = req.body; //extraigo valores
    const nuevoAlumno = new Alumno({
        nombre: nombre,
        apellido: apellido,
        legajo: legajo,
        curso: curso
    });
    await nuevoAlumno.save();
    res.json(nuevoAlumno)
};

controladorAlumno.obtenerAlumno = async (req, res) => {
        const alumnoEncontrado = await Alumno.findById(req.params.id)
        res.json({alumnoEncontrado})
};

controladorAlumno.modificarAlumno = async (req, res) => {
    const alumnoModificado = await Alumno.findByIdAndUpdate(req.params.id, req.body);
    res.json({alumnoModificado})
};

controladorAlumno.borrarAlumno = async (req, res) => {
    await Alumno.findByIdAndDelete(req.params.id, req.body);
    res.json({mensaje: 'Alumno deletiado'})
};

module.exports = controladorAlumno;

